import styled from "styled-components";

const Header = styled.h1`
    margin: 0;
    font-size: 7em;
    font-family: 'Montserrat';
    color: white;
`;

export default Header;