import styled from "styled-components";

const Subheader = styled.h1`
    margin: 0;
    line-height: 1;
    font-size: 3em;
    font-family: 'Montserrat';
    color: white;
`;

export default Subheader;