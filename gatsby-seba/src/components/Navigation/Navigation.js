import React from 'react';
import styled from 'styled-components';
import { Link, link } from 'gatsby';

const NavigationWrapper = styled.ul`
    padding: 0;
    list-style: none;
    color: white;
    font-family: 'Montserrat';
    font-weight: 700;
    display: flex;

`;


const NavigationItem = styled.li`
    margin-left: 15px;
    position: relative;

    ::after {
        position: absolute;
        top: 0;
        right: -9px;
        content: '';
        display: block;
        height: 100%;
        width: 2px;
        background: white;
    }

    :last-child::after {
        display: none;
    }
`;


const Styledlink = styled(Link)`
    color: white;
    text-decoration: none;
`;


const Navigation = () => (
    <NavigationWrapper>
        <NavigationItem>
            <Styledlink to={'../gosia/'}>Gosia</Styledlink>
        </NavigationItem>
        <NavigationItem>
            <Styledlink to={'../maks/'}>Maks</Styledlink>
        </NavigationItem>
        <NavigationItem>
            <Styledlink to={'../oskar/'}>Oskar</Styledlink>
        </NavigationItem>
    </NavigationWrapper>
);

export default Navigation;