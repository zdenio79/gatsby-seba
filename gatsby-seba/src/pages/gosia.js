import React from "react"
import { Link } from 'gatsby'

const IndexPage = () => (
  <>
    <h1>Strona Gosi </h1>
    <p>Serdecznie zapraszamy.</p>
    <Link to={'/'}>Powrót</Link>
  </>
)

export default IndexPage