import React from "react"

const NotFoundPage = () => (
  <>
    
    <h1>Strona nie istnieje </h1>
    <p>Przepraszamy za utrudnienia</p>
  </>
)

export default NotFoundPage
