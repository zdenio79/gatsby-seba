import React from "react"
import { Link } from 'gatsby'

const IndexPage = () => (
  <>
    <h1>Strona Oskara </h1>
    <p>Serdecznie zapraszamy.</p>
    <Link to={'/'}>Powrót</Link>
  </>
)

export default IndexPage