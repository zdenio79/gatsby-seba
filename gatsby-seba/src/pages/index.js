import React from "react"
import styled, { createGlobalStyle } from "styled-components"
import { Link } from "gatsby"
import Header from "../components/Header/Header"
import Subheader from "../components/Subheader/Subheader"
import Navigation from "../components/Navigation/Navigation"
import backgroundImage from "../images/jesien.jpg"


const GlobalStyle = createGlobalStyle`
  body {
    padding: 0;
    margin: 0;
  }
  *. *::befor, *::after {
    box-sizing: border-box;
  }
`;


const StyledWrapper = styled.div`
  display: flex;
  flex-direction: center;
  justify-content: center;
  padding: 20vh;
  width: 100%;
  heigth: 100vh;
  background: url(${backgroundImage});
  background-size: cover;

`;

const IndexPage = () => (
  <>
    <GlobalStyle />
    <StyledWrapper>
      <Header>Hello Seba</Header>
      <Subheader>+ zwierzęta</Subheader>
      <Navigation />
    </StyledWrapper>
  </>
)

export default IndexPage
